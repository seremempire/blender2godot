#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import bpy


class Blender2GodotPreferences(bpy.types.AddonPreferences):
    bl_idname = "blender2godot"
    godot_executable_path: bpy.props.StringProperty(
        name="Godot Executable Path",
        description="Set the current Godot executable path on your system",
        default="/usr/local/games/godot-engine",
    )

    def draw(self, context):
        layout = self.layout

        layout.label(
            text="Here you can set the system properties")

        split = layout.split(factor=0.25)

        col = split.column()
        sub = col.column(align=True)
        sub.prop(self, "godot_executable_path")
        

def register():
    bpy.utils.register_class(Blender2GodotPreferences)


def unregister():
    bpy.utils.unregister_class(Blender2GodotPreferences)
