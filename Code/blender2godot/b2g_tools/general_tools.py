#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

"""
Building utils for exporting to different platforms
"""

import bpy


class ExportProjectToGodotOperator(bpy.types.Operator):
    """Export Project To Godot Operator"""
    bl_idname = "scene.export_project_to_godot_operator"
    bl_label = "Export To Godot"
    
    def execute(self, context):
        print("Deleting last export...")
        bpy.ops.scene.delete_project_operator()
        print("Last export deleted!")
        print("Exporting to godot project...")
        bpy.ops.scene.create_godot_project_operator()
        bpy.ops.scene.export_game_operator()
        bpy.ops.scene.open_godot_project_operator()
        print("Project exported!")
        return {'FINISHED'}


def register():
    bpy.utils.register_class(ExportProjectToGodotOperator)

def unregister():
    bpy.utils.unregister_class(ExportProjectToGodotOperator)

