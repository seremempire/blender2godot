bl_info = {
    "name": "Blender2Godot",
    "author": "Jaume Castells",
    "version": (0,1),
    "blender": (2, 82, 0),
    "category": "Object",
}

import os
import subprocess
import shutil
import json
from fileinput import FileInput
import requests

import bpy

class my_dictionary(dict): 
  
    # __init__ function 
    def __init__(self): 
        self = dict() 
          
    # Function to add key:value 
    def add(self, key, value): 
        self[key] = value 

class Blender2GodotPanel(bpy.types.Panel):
    """Blender2Godot Panel"""
    bl_label = "Blender2Godot v0.1"
    bl_idname = "BLENDER2GODOT_PT_layout"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Blender2Godot"
    
    def draw(self, context):
        layout = self.layout

        scene = context.scene
        blend_data = context.blend_data
        row = layout.row()
        row.label(text="Addon properties:")
        row = layout.row()
        box0 = row.box()
        
        # Addon settings
        #box0.prop(scene, "custom_godot")
        #if scene.custom_godot:
        box0.prop(scene, "godot_executable")

class AreYouSureDeletingOperator(bpy.types.Operator):
    """Really?"""
    bl_idname = "scene.are_you_sure_deleting_operator"
    bl_label = "Are you sure?"
    
    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        bpy.ops.scene.delete_project_operator()
        self.report({'INFO'}, "Project deleted!")
        print("Project deleted.")
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)

class B2G_ToolsPanel(bpy.types.Panel):
    """B2G Tools Panel"""
    bl_label = "B2G Tools"
    bl_idname = "B2GTOOLS_PT_layout"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Blender2Godot"
    
    advanced_tools = False
    
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        blend_data = context.blend_data
        
        # Export project to godot button
        row = layout.row()
        row.scale_y = 3.0
        row.operator("scene.export_project_to_godot_operator")        
        
        # Test game button
        row = layout.row()
        row.scale_y = 3.0
        row.operator("scene.test_game_operator")
        
        row = layout.row()
        row.prop(context.scene, 'advanced_tools')
        
        if context.scene.advanced_tools:
            # Delete project button
            row = layout.row()
            row.scale_y = 3.0
            row.operator_context = 'INVOKE_DEFAULT' # TODO: not working!!!
            #row.operator("scene.delete_project_button_operator")
            row.operator("scene.delete_project_operator")
            
            # Create project button
            row = layout.row()
            row.scale_y = 3.0
            row.operator("scene.create_godot_project_operator")
            
            # Export project button
            #row = layout.row()
            #row.scale_y = 3.0
            #row.operator("scene.export_game_operator")
            
            # Open godot project button
            row = layout.row()
            row.scale_y = 3.0
            row.operator("scene.open_godot_project_operator")

class BuildGameOperator(bpy.types.Operator):
    """Build Game Operator"""
    bl_idname = "scene.build_game_operator"
    bl_label = "Build Game"
    
    export_presets_dir_path = ""
    total_file_content = []
    android_preset_content = []
    linux_preset_content = []
    windows_preset_content = []
    mac_preset_content = []
    web_preset_content = []
    exes_dirname = "builds"
    android_exe_dirname = "android_build"
    linux_exe_dirname = "linux_build"
    windows_exe_dirname = "windows_build"
    mac_exe_dirname = "mac_build"
    web_exe_dirname = "web_build"
    
    def add_android_preset(self, context):
        self.android_preset_content.clear()
        self.android_preset_filepath = os.path.join(self.export_presets_dir_path, "android_preset.cfg")
        preset_file = open(self.android_preset_filepath, "r")
        self.android_preset_content = preset_file.readlines()
        preset_file.close()
    
    def add_linux_preset(self, context):
        self.linux_preset_content.clear()
        self.linux_preset_filepath = os.path.join(self.export_presets_dir_path, "linux_preset.cfg")
        preset_file = open(self.linux_preset_filepath, "r")
        self.linux_preset_content = preset_file.readlines()
        preset_file.close()

    def add_mac_preset(self, context):
        self.mac_preset_content.clear()
        self.mac_preset_filepath = os.path.join(self.export_presets_dir_path, "mac_preset.cfg")
        preset_file = open(self.mac_preset_filepath, "r")
        self.mac_preset_content = preset_file.readlines()
        preset_file.close()
    
    def add_preset_content(self, context, preset_content, preset_number):
        for n_line in range(0, len(preset_content)):
            phrase = preset_content[n_line]
            if phrase.startswith("[preset.") and not phrase.endswith("options]\n"):
                new_phrase = "[preset." + str(preset_number) + "]"
                phrase = new_phrase
            if phrase.startswith("[preset.") and phrase.endswith("options]\n"):
                new_phrase = "[preset." + str(preset_number) + ".options]"
                phrase = new_phrase
            self.total_file_content.append(phrase)
        self.total_file_content.append("\n")
    
    def add_selected_export_presets(self, context):
        self.total_file_content.clear()
        self.android_preset_content.clear()
        self.linux_preset_content.clear()
        self.windows_preset_content.clear()
        self.mac_preset_content.clear()
        self.web_preset_content.clear()
        n_versions = 0
        if context.scene.android_export:
            print("Android version...OK")
            self.add_android_preset(context)
            n_versions += 1
        if context.scene.linux_export:
            print("Linux version...OK")
            self.add_linux_preset(context)
            n_versions += 1
        if context.scene.windows_export:
            print("Windows version...OK")
            self.add_windows_preset(context)
            n_versions += 1
        if context.scene.mac_export:
            print("Mac version...OK")
            self.add_mac_preset(context)
            n_versions += 1
        if context.scene.web_export:
            print("Web version...OK")
            self.add_web_preset(context)
            n_versions += 1
        print(n_versions, "versions to export")
        self.config_presets(context)
        print(self.total_file_content)
        self.fill_total_content(context)
        self.write_export_presets(context)        
    
    def add_web_preset(self, context):
        self.web_preset_content.clear()
        self.web_preset_filepath = os.path.join(self.export_presets_dir_path, "web_preset.cfg")
        preset_file = open(self.web_preset_filepath, "r")
        self.web_preset_content = preset_file.readlines()
        preset_file.close()

    def add_windows_preset(self, context):
        self.windows_preset_content.clear()
        self.windows_preset_filepath = os.path.join(self.export_presets_dir_path, "windows_preset.cfg")
        preset_file = open(self.windows_preset_filepath, "r")
        self.windows_preset_content = preset_file.readlines()
        preset_file.close()
    
    def build_game(self, context):
        print("Building game...")
        print("Creating presets...")
        self.add_selected_export_presets(context)
        print("Compiling...")
        self.compile_selected_exporting(context)
        
    def compile_selected_exporting(self, context):
        self.builds_path = os.path.join(context.scene.game_folder, self.exes_dirname)
        if not os.path.isdir(self.builds_path):
            os.mkdir(self.builds_path)
        if context.scene.android_export:
            print("Android version...OK")
        if context.scene.linux_export:
            linux_dir_path = os.path.join(self.builds_path, self.linux_exe_dirname)
            if not os.path.isdir(linux_dir_path):
                os.mkdir(linux_dir_path)
            print("Linux version...OK")
            linux_filepath = os.path.join(linux_dir_path, context.scene.game_name)
            self.cmd = subprocess.Popen([bpy.path.abspath(context.scene.godot_executable), "--path", context.scene.project_folder, "--export", "Linux/X11", linux_filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if context.scene.windows_export:
            windows_dir_path = os.path.join(self.builds_path, self.windows_exe_dirname)
            if not os.path.isdir(windows_dir_path):
                os.mkdir(windows_dir_path)
            print("Windows version...OK")
            windows_filepath = os.path.join(windows_dir_path, context.scene.game_name)
            self.cmd = subprocess.Popen([bpy.path.abspath(context.scene.godot_executable), "--path", context.scene.project_folder, "--export", "Windows Desktop", windows_filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if context.scene.mac_export:
            mac_dir_path = os.path.join(self.builds_path, self.mac_exe_dirname)
            if not os.path.isdir(mac_dir_path):
                os.mkdir(mac_dir_path)
            print("Mac version...OK")
            mac_filepath = os.path.join(mac_dir_path, context.scene.game_name)
            self.cmd = subprocess.Popen([bpy.path.abspath(context.scene.godot_executable), "--path", context.scene.project_folder, "--export", "Mac OSX", mac_filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if context.scene.web_export:
            web_dir_path = os.path.join(self.builds_path, self.web_exe_dirname)
            if not os.path.isdir(web_dir_path):
                os.mkdir(web_dir_path)
            print("Web version...OK")
            web_filepath = os.path.join(web_dir_path, context.scene.game_name)
            self.cmd = subprocess.Popen([bpy.path.abspath(context.scene.godot_executable), "--path", context.scene.project_folder, "--export", "HTML5", web_filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            
    def config_presets(self, context):
        # Config all presets paths and properties on self.<content> files
        self.game_exports_path = os.path.join(context.scene.game_folder, self.exes_dirname)
        # Android
        android_exports_path = os.path.join(self.game_exports_path, self.android_exe_dirname)
        for n_line in range(0, len(self.android_preset_content)):
            if self.android_preset_content[n_line].find("export_path") > -1:
                self.android_preset_content[n_line] = 'export_path="' + android_exports_path + '"\n'
        # Linux
        linux_exports_path = os.path.join(self.game_exports_path, self.linux_exe_dirname)
        for n_line in range(0, len(self.linux_preset_content)):
            if self.linux_preset_content[n_line].find("export_path") > -1:
                self.linux_preset_content[n_line] = 'export_path="' + linux_exports_path + '"\n'          
        # Windows
        windows_exports_path = os.path.join(self.game_exports_path, self.windows_exe_dirname)
        for n_line in range(0, len(self.windows_preset_content)):
            if self.windows_preset_content[n_line].find("export_path") > -1:
                self.windows_preset_content[n_line] = 'export_path="' + windows_exports_path + '"\n'
        # Mac
        mac_exports_path = os.path.join(self.game_exports_path, self.mac_exe_dirname)
        for n_line in range(0, len(self.mac_preset_content)):
            if self.mac_preset_content[n_line].find("export_path") > -1:
                self.mac_preset_content[n_line] = 'export_path="' + mac_exports_path + '"\n'
        # Web
        web_exports_path = os.path.join(self.game_exports_path, self.web_exe_dirname)
        for n_line in range(0, len(self.web_preset_content)):
            if self.web_preset_content[n_line].find("export_path") > -1:
                self.web_preset_content[n_line] = 'export_path="' + web_exports_path + '"\n'
    
    def fill_total_content(self, context):
        n_preset = 0
        if len(self.android_preset_content) > 0:
            self.add_preset_content(context, self.android_preset_content, n_preset)
            n_preset += 1
        if len(self.linux_preset_content) > 0:
            self.add_preset_content(context, self.linux_preset_content, n_preset)
            n_preset += 1
        if len(self.windows_preset_content) > 0:
            self.add_preset_content(context, self.windows_preset_content, n_preset)
            n_preset += 1
        if len(self.mac_preset_content) > 0:
            self.add_preset_content(context, self.mac_preset_content, n_preset)
            n_preset += 1
        if len(self.web_preset_content) > 0:
            self.add_preset_content(context, self.web_preset_content, n_preset)
            n_preset += 1
        
    def find_preset_dir_path(self, context):
        possible_paths = [os.path.join(bpy.utils.resource_path("USER"), "addons", "blender2godot"),
        os.path.join(bpy.utils.resource_path("LOCAL"), "addons", "blender2godot"),
        "/home/zamme/ViloStudio/Blender2Godot/Code/blender2godot/exporting_presets"]
        for p_path in possible_paths:
            if os.path.isdir(p_path):
                self.export_presets_dir_path = p_path
        print("Export presets directory:", self.export_presets_dir_path)
    
    def write_export_presets(self, context):
        os.remove(self.export_presets_filepath)
        self.presets_file = open(self.export_presets_filepath, "w")
        self.presets_file.writelines(self.total_file_content)
        
    def main(self, context):
        self.find_preset_dir_path(context)
        self.export_presets_filepath = os.path.join(context.scene.project_folder, "export_presets.cfg")
        self.build_game(context)        

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}

class CreateGodotProjectOperator(bpy.types.Operator):
    """Create Godot Project Operator"""
    bl_idname = "scene.create_godot_project_operator"
    bl_label = "Create Project"
    
    project_file_joined_path = ""
    project_file = None
    godot_project_template_path = ""
    
    def add_initial_files(self, context):
        self.project_folder_joined_path = os.path.join(context.scene.game_folder, context.scene.game_name + "_Game")
        if os.path.isdir(self.project_folder_joined_path):
            print("Folder exists...")
        else:
            print("Creating project tree from template...")
            shutil.copytree(self.godot_project_template_path, self.project_folder_joined_path)
            print("Godot project tree created.")
    
    def find_project_template_dir_path(self, context):
        possible_paths = [os.path.join(bpy.utils.resource_path("USER"), "addons", "blender2godot", "project_template"),
        os.path.join(bpy.utils.resource_path("LOCAL"), "addons", "blender2godot", "project_template"),
        "/home/zamme/ViloStudio/Blender2Godot/Code/blender2godot/project_template"]
        for p_path in possible_paths:
            if os.path.isdir(p_path):
                self.godot_project_template_path = p_path
        print("Godot project template directory:", self.godot_project_template_path)
        
    def modify_godot_project_file(self, context):
        context.scene.godot_project_filepath = os.path.join(self.project_folder_joined_path, "project.godot")
        godot_project_file = open(context.scene.godot_project_filepath, "r")
        godot_project_file_content = godot_project_file.readlines()
        godot_project_file.close()
        #print("Content:", godot_project_file_content)
        #print("Lines:", len(godot_project_file_content))
        # Title
        godot_project_file_content = self.replace(context, godot_project_file_content, "Godot_Project-name", context.scene.game_name)
        #print(godot_project_file_content)
        # Save File
        os.remove(context.scene.godot_project_filepath)
        f = open(context.scene.godot_project_filepath, "w")
        f.writelines(godot_project_file_content)
        f.close()
    
    def modify_project_information(self, context):
        self.modify_godot_project_file(context)
    
    def replace(self, context, where, this, that):
        for n_line in range(0, len(where)):
            if where[n_line].find(this) > -1:
                #print("Located")
                where[n_line] = where[n_line].replace(this, that)
                #print("Replaced by", that)
        return where
                
    def main(self, context):
        self.find_project_template_dir_path(context)
        context.scene.project_folder = os.path.join(context.scene.game_folder, context.scene.game_name + "_Game")
        self.add_initial_files(context)
        self.modify_project_information(context)

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}

class DeleteProjectButtonOperator(bpy.types.Operator): # TODO: PENDING TO FIX PATHS!
    """Delete Project Button Operator"""
    bl_idname = "scene.delete_project_button_operator"
    bl_label = "Delete Project"
    
    def delete_project(self, context):
        if os.path.isdir(context.scene.project_folder):
            print("Deleting project...", self.project_folder_joined_path)
            bpy.ops.scene.are_you_sure_deleting_operator()
            print("Project deleted")
        else:
            print("Project not found.")
    
    def main(self, context):
        self.project_folder_joined_path = os.path.join(context.scene.game_folder, context.scene.game_name + "_Game")
        self.delete_project(context)        

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}

class DeleteProjectOperator(bpy.types.Operator):
    """Delete Project Operator"""
    bl_idname = "scene.delete_project_operator"
    bl_label = "Delete Project"
    
    def delete_project(self, context):
        if os.path.isdir(context.scene.project_folder):
            print("Deleting project...", context.scene.project_folder)
            shutil.rmtree(context.scene.project_folder)
            print("Project deleted")
        else:
            print("Project not found.")
        executables_path = os.path.join(context.scene.game_folder, "builds")
        if os.path.isdir(executables_path):
            print("Deleting executables...", executables_path)
            shutil.rmtree(executables_path)
            print("Executables deleted")
        else:
            print("Executables not found.")
    
    def main(self, context):
        self.delete_project(context)        

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}

'''
class DownloadGodotOperator(bpy.types.Operator):
    """Download Godot"""
    bl_idname = "scene.download_godot_operator"
    bl_label = "Download Godot"
    
    def execute(self, context):
        print("Downloading Godot...")
        url = "https://downloads.tuxfamily.org/godotengine/3.2.2/Godot_v3.2.2-stable_x11.64.zip"
        save_path = context.scene.godot_executable_downloaded_zip
        with urllib.request.urlopen(url) as response, open(save_path, "wb") as out_file:
        shutil.copyfileobj(response, out_file)
        
        return {"FINISHED"}

    #def invoke(self, context, event):
        #return context.window_manager.invoke_confirm(self, event)
'''

class ExportGameOperator(bpy.types.Operator):
    """Export Game Operator"""
    bl_idname = "scene.export_game_operator"
    bl_label = "Export Game"
    
    assets_folder_name = "assets"
    models_folder_name = "models"
    colliders_filepath = ""
    player_info_filepath = ""
    lights_info_filepath = ""
    
    dict_colliders = my_dictionary()
    dict_player_info = my_dictionary()
    dict_lights_info = my_dictionary()
    
    def export_colliders(self, context):
        print("Exporting colliders...")
        self.find_colliders_file_path(context)
        scene_objects = context.scene.objects
        for ob in scene_objects:
            print("Exporting", ob.name)
            self.dict_colliders.add(ob.name, ob.collider)
        print("Colliders exporting finished.")
        self.data_colliders = json.dumps(self.dict_colliders, indent=1, ensure_ascii=True)
        with open(self.colliders_filepath, 'w') as outfile:
            outfile.write(self.data_colliders + '\n')
        #with open(self.colliders_filepath, 'r') as fp:
            #data_file = json.load(fp)
        #print(data_file)
    
    def export_game_project(self, context):
        print("Exporting game", context.scene.project_folder)
        self.models_folder_path = os.path.join(self.assets_folder_path, self.models_folder_name)
        if not os.path.isdir(self.models_folder_path):
            os.mkdir(self.models_folder_path)
        self.export_scene(context)
        self.export_colliders(context)
        self.export_player_info(context)
        self.export_lights(context)
        bpy.ops.scene.set_godot_project_environment_operator()
    
    def export_lights(self, context):
        print("Exporting lights...")
        self.find_lights_file_path(context)
        scene_objects = context.scene.objects
        for ob in scene_objects:
            #print(ob.type)
            if ob.type == "LIGHT":
                if ob.data.type == "POINT": # TODO: all light types?
                    print("Exporting", ob.name)
                    self.dict_lights_info.add(ob.name, ob.data.type)
                    self.dict_lights_info.add(ob.name + "PositionX", ob.location.x)
                    self.dict_lights_info.add(ob.name + "PositionY", ob.location.y)
                    self.dict_lights_info.add(ob.name + "PositionZ", ob.location.z)
                    self.dict_lights_info.add(ob.name + "Energy", ob.data.energy)
                    self.dict_lights_info.add(ob.name + "Range", ob.data.distance)
                    self.dict_lights_info.add(ob.name + "ColorR", ob.data.color[0])
                    self.dict_lights_info.add(ob.name + "ColorG", ob.data.color[1])
                    self.dict_lights_info.add(ob.name + "ColorB", ob.data.color[2])
        print("Lights exporting finished.")
        self.data_lights = json.dumps(self.dict_lights_info, indent=1, ensure_ascii=True)
        with open(self.lights_info_filepath, 'w') as outfile:
            outfile.write(self.data_lights + '\n')
    
    def export_player_info(self, context):
        print("Exporting player...")
        self.find_player_info_file_path(context)
        self.dict_player_info.add("GravityOn", context.scene.player_gravity_on)
        player_position = context.scene.objects["Camera"].location
        player_rotation = context.scene.objects["Camera"].rotation_euler
        self.dict_player_info.add("InitialPositionX", player_position[0])
        self.dict_player_info.add("InitialPositionY", player_position[1])
        self.dict_player_info.add("InitialPositionZ", player_position[2])
        self.dict_player_info.add("InitialRotationX", player_rotation[0])
        self.dict_player_info.add("InitialRotationY", player_rotation[1])
        self.dict_player_info.add("InitialRotationZ", player_rotation[2])
        self.data_player_info = json.dumps(self.dict_player_info, indent=1, ensure_ascii=True)
        with open(self.player_info_filepath, 'w') as outfile:
            outfile.write(self.data_player_info + '\n')
        with open(self.player_info_filepath, 'r') as fp:
            data_file = json.load(fp)
        print(data_file)        
    
    def export_scene(self, context):
        print("Exporting scene", context.scene.name)
        model_path = os.path.join(self.models_folder_path, context.scene.name)
        bpy.ops.export_scene.gltf(filepath=model_path)
        print("Scene", context.scene.name, "exported.")
    
    def find_colliders_file_path(self, context):
        possible_paths = [os.path.join(bpy.utils.resource_path("USER"), "addons", "blender2godot", "project_template", "colliders_info", "colliders.json"),
        os.path.join(bpy.utils.resource_path("LOCAL"), "addons", "blender2godot", "project_template", "colliders_info", "colliders.json"),        os.path.join(context.scene.project_folder, "colliders_info", "colliders.json")]
        for p_path in possible_paths:
            if os.path.isfile(p_path):
                self.colliders_filepath = p_path
        print("Colliders json filepath:", self.colliders_filepath)
    
    def find_lights_file_path(self, context):
        possible_paths = [os.path.join(bpy.utils.resource_path("USER"), "addons", "blender2godot", "project_template", "lights_info", "lights_info.json"),
        os.path.join(bpy.utils.resource_path("LOCAL"), "addons", "blender2godot", "project_template", "colliders_info", "colliders.json"),        os.path.join(context.scene.project_folder, "lights_info", "lights_info.json")]
        for p_path in possible_paths:
            if os.path.isfile(p_path):
                self.lights_info_filepath = p_path
        print("Lights json filepath:", self.lights_info_filepath)
        
    def find_player_info_file_path(self, context):
        possible_paths = [os.path.join(bpy.utils.resource_path("USER"), "addons", "blender2godot", "project_template", "player_info", "player_info.json"),
        os.path.join(bpy.utils.resource_path("LOCAL"), "addons", "blender2godot", "project_template", "player_info", "player_info.json"),        os.path.join(context.scene.project_folder, "player_info", "player_info.json")]
        for p_path in possible_paths:
            if os.path.isfile(p_path):
                self.player_info_filepath = p_path
        print("Player info json filepath:", self.player_info_filepath)
    
    def main(self, context):
        self.assets_folder_path = os.path.join(context.scene.project_folder, self.assets_folder_name)
        if not os.path.isdir(self.assets_folder_path):
            os.mkdir(self.assets_folder_path)
        self.export_game_project(context)        

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}

class ExportProjectToGodotOperator(bpy.types.Operator):
    """Export Project To Godot Operator"""
    bl_idname = "scene.export_project_to_godot_operator"
    bl_label = "Export To Godot"
    
    def execute(self, context):
        print("Deleting last export...")
        bpy.ops.scene.delete_project_operator()
        print("Last export deleted!")
        print("Exporting to godot project...")
        bpy.ops.scene.create_godot_project_operator()
        bpy.ops.scene.export_game_operator()
        bpy.ops.scene.open_godot_project_operator()
        print("Project exported!")
        return {'FINISHED'}

class GameExportPanel(bpy.types.Panel):
    """Game Export Panel"""
    bl_label = "Game Export"
    bl_idname = "GAMEEXPORT_PT_layout"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Blender2Godot"
    bl_options = {"DEFAULT_CLOSED"}
    
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        blend_data = context.blend_data
        
        # Export platforms box
        row = layout.row()
        box2 = row.box()
        box2.label(text="Export platforms:")
        box2.label(text="Be sure that godot export templates are installed!", icon="QUESTION")
        box2.prop(scene, "linux_export")
        box2.prop(scene, "windows_export")
        box2.prop(scene, "mac_export")
        box2.prop(scene, "web_export")
        #box2.prop(scene, "android_export")
        #if scene.android_export:
            
        # Build game button
        row = layout.row()
        row.scale_y = 3.0
        row.operator("scene.build_game_operator")

class GodotProjectPropertiesPanel(bpy.types.Panel):
    """Godot Project Properties Panel"""
    bl_label = "Godot Project Properties"
    bl_idname = "GODOTPROJECTPROPERTIES_PT_layout"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Blender2Godot"
    
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        blend_data = context.blend_data
        
        row = layout.row()
        row.label(text="Project properties:")
        row = layout.row()
        box1 = row.box()
        # Project properties box
        box1.prop(scene, "game_name")
        box1.prop(scene, "game_folder")
        box1.prop(scene, "game_icon")

class OpenGodotProjectOperator(bpy.types.Operator): # It DOESN'T block blender execution until game exits
    """Open Godot Project Operator"""
    bl_idname = "scene.open_godot_project_operator"
    bl_label = "Open Godot Project"
    
    def execute(self, context):
        print("Opening godot project...")
        self.cmd = subprocess.Popen([bpy.path.abspath(context.scene.godot_executable), "--editor", "--path", context.scene.project_folder], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        return {'FINISHED'}

class PlayerPropertiesPanel(bpy.types.Panel):
    """Player Properties Panel"""
    bl_label = "Player Properties"
    bl_idname = "PLAYERPROPERTIES_PT_layout"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Blender2Godot"
    bl_options = {"DEFAULT_CLOSED"}
    
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        blend_data = context.blend_data
        
        # INITIAL PROPERTIES
        row = layout.row()
        box = row.box()
        box.prop(scene, "player_gravity_on")

class ScenePropertiesPanel(bpy.types.Panel):
    """Scene Properties Panel"""
    bl_label = "Scene Properties"
    bl_idname = "SCENEPROPERTIES_PT_layout"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Blender2Godot"
    bl_options = {"DEFAULT_CLOSED"}
    
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        blend_data = context.blend_data
        
        # SCENE PROPERTIES
        # Environment Lighting
        # Sky
        row = layout.row()
        row.label(text="Environment properties:")
        row = layout.row()
        box = row.box()
        box.prop(context.scene, "sky_on")
        row1 = box.row()
        if context.scene.sky_on:
            box1 = row1.box()
            box1.prop(context.scene, "sky_energy")
        
        # ACTIVE OBJECT PROPERTIES
        row = layout.row()
        row.label(text="Active object properties:")
        row = layout.row()
        box3 = row.box()
        if context.active_object is not None:
            box3.label(text=context.active_object.name)
            box3.prop(context.active_object, "collider")

class SetGodotProjectEnvironmentOperator(bpy.types.Operator):
    """Set Godot Project Environment Operator"""
    bl_idname = "scene.set_godot_project_environment_operator"
    bl_label = "Set Godot Project Environment"
    
    background_mode_line = "background_mode = "
    background_energy_line = "background_energy = "
    done = False
    
    def replace_or_fill(self, context, where, this, that, pre_sentence):
        self.done = False
        for n_line in range(0, len(where)):
            if where[n_line].find(this) > -1:
                where[n_line] = that
                self.done = True
                print("First: ", that)
        if not self.done:
            for n_line in range(0, len(where)):
                if where[n_line].find(pre_sentence) > -1:
                    where.insert(n_line+1, that)
                    print("Second: ", that)
        return where
    
    def set_sky(self, context):
        print("Setting sky...")
        default_environment_filepath = os.path.join(context.scene.project_folder, "default_env.tres")
        default_environment_file = open(default_environment_filepath, "r")
        default_environment_file_content = default_environment_file.readlines()
        default_environment_file.close()
        
        if context.scene.sky_on:
            new_background_mode_line = self.background_mode_line + "2" + "\n"
            new_background_energy_line = self.background_energy_line + str(context.scene.sky_energy) + "\n"
            default_environment_file_content = self.replace_or_fill(context, default_environment_file_content, self.background_mode_line, new_background_mode_line, "[resource]")
        
            default_environment_file_content = self.replace_or_fill(context, default_environment_file_content, self.background_energy_line, new_background_energy_line, "background_sky =")
        else:
            new_background_mode_line = self.background_mode_line + "0" + "\n"
            default_environment_file_content = self.replace_or_fill(context, default_environment_file_content, self.background_mode_line, new_background_mode_line, "[resource]")        
        
        # Save File
        os.remove(default_environment_filepath)
        f = open(default_environment_filepath, "w")
        f.writelines(default_environment_file_content)
        f.close()
        
        print("Sky done. ", new_background_mode_line)
        
    def execute(self, context):
        print("Setting Godot project environment...")
        self.set_sky(context)
        return {'FINISHED'}

class TestGameOperator(bpy.types.Operator): # It blocks blender execution until game exits
    """Test Game Operator"""
    bl_idname = "scene.test_game_operator"
    bl_label = "Test Game"
    
    project_folder_joined_path = ""
    
    def start_game(self, context):
        print("Starting game", self.project_folder_joined_path)
        subprocess.call([bpy.path.abspath(context.scene.godot_executable), "--path", context.scene.project_folder])
    
    def main(self, context):
        self.start_game(context)        

    def execute(self, context):
        self.main(context)
        return {'FINISHED'}


def init_properties():
    bpy.types.Scene.game_name = bpy.props.StringProperty(name="Name", default="NEW_GAME")
    bpy.types.Scene.game_folder = bpy.props.StringProperty(name="Folder", subtype="DIR_PATH", default="./")
    bpy.types.Scene.game_icon = bpy.props.StringProperty(name="Icon", subtype="FILE_PATH", default=".")
    bpy.types.Scene.project_folder = bpy.props.StringProperty(name="Project Folder", subtype="DIR_PATH", default="./")
    
    bpy.types.Scene.godot_executable = bpy.props.StringProperty(name="Godot", subtype="FILE_PATH", default="/usr/local/games/godot-engine")
    #bpy.types.Scene.custom_godot = bpy.props.BoolProperty(name="Custom Godot", default=False)
    #bpy.types.Scene.godot_executable_downloaded_zip = bpy.props.StringProperty(name="Godot zip", subtype="FILE_PATH", default=".")
    
    bpy.types.Scene.colliders_filepath = bpy.props.StringProperty(name="Colliders", subtype="FILE_PATH", default=".")
    
    bpy.types.Scene.android_template_filepath = bpy.props.StringProperty(name="Android Template", subtype="FILE_PATH", default=".")
    
    bpy.types.Scene.godot_project_filepath = bpy.props.StringProperty(name="GPF", subtype="FILE_PATH", default=".")

    bpy.types.Scene.android_export = bpy.props.BoolProperty(name="Android", default=False)
    bpy.types.Scene.linux_export = bpy.props.BoolProperty(name="Linux", default=False)
    bpy.types.Scene.windows_export = bpy.props.BoolProperty(name="Windows", default=False)
    bpy.types.Scene.mac_export = bpy.props.BoolProperty(name="Mac", default=False)
    bpy.types.Scene.web_export = bpy.props.BoolProperty(name="Web", default=False)
    
    bpy.types.Object.collider = bpy.props.BoolProperty(name="Collider", default=True)
    bpy.types.Scene.advanced_tools = bpy.props.BoolProperty(name="Advanced Tools", default=False)

    bpy.types.Scene.player_gravity_on = bpy.props.BoolProperty(name="Gravity", default=True)
    
    # Environment properties
    bpy.types.Scene.sky_on = bpy.props.BoolProperty(name="Sky", default=True)
    bpy.types.Scene.sky_energy = bpy.props.FloatProperty(name="Sky Energy", default=1.0, min=0.0, max=16.0, soft_min=0.0, soft_max=16.0)
    
def clear_properties():
    pass
    
def register():
    init_properties()
    bpy.utils.register_class(SetGodotProjectEnvironmentOperator)
    bpy.utils.register_class(ExportProjectToGodotOperator)
    bpy.utils.register_class(BuildGameOperator)
    bpy.utils.register_class(DeleteProjectButtonOperator)
    bpy.utils.register_class(AreYouSureDeletingOperator)
    bpy.utils.register_class(DeleteProjectOperator)
    bpy.utils.register_class(ExportGameOperator)
    bpy.utils.register_class(OpenGodotProjectOperator)
    bpy.utils.register_class(TestGameOperator)
    #bpy.utils.register_class(SetGameFolderOperator)
    bpy.utils.register_class(CreateGodotProjectOperator)
    bpy.utils.register_class(Blender2GodotPanel)
    bpy.utils.register_class(GodotProjectPropertiesPanel)
    bpy.utils.register_class(ScenePropertiesPanel)
    bpy.utils.register_class(PlayerPropertiesPanel)
    bpy.utils.register_class(B2G_ToolsPanel)
    bpy.utils.register_class(GameExportPanel)
    print("Blender2Godot addon loaded.")


def unregister():
    bpy.utils.unregister_class(PlayerPropertiesPanel)
    bpy.utils.unregister_class(GameExportPanel)
    bpy.utils.unregister_class(B2G_ToolsPanel)
    bpy.utils.unregister_class(ScenePropertiesPanel)
    bpy.utils.unregister_class(GodotProjectPropertiesPanel)
    bpy.utils.unregister_class(Blender2GodotPanel)
    bpy.utils.unregister_class(CreateGodotProjectOperator)
    #bpy.utils.unregister_class(SetGameFolderOperator)
    bpy.utils.unregister_class(TestGameOperator)
    bpy.utils.unregister_class(OpenGodotProjectOperator)
    bpy.utils.unregister_class(ExportGameOperator)
    bpy.utils.unregister_class(DeleteProjectOperator)
    bpy.utils.unregister_class(AreYouSureDeletingOperator)
    bpy.utils.unregister_class(DeleteProjectButtonOperator)
    bpy.utils.unregister_class(BuildGameOperator)
    bpy.utils.unregister_class(ExportProjectToGodotOperator)
    bpy.utils.unregister_class(SetGodotProjectEnvironmentOperator)
    clear_properties()
    print("Blender2Godot addon unloaded.")


if __name__ == "__main__":
    register()



'''
class SetGameFolderOperator(bpy.types.Operator):
    bl_idname = "scene.set_game_folder_operator"
    bl_label = "Set game folder"
    directory = bpy.props.StringProperty(subtype="DIR_PATH")

    def execute(self, context):
        print(self.directory)
        scene.game_folder = self.directory
        return {'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
'''
